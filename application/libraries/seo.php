<?php

/**
 * Seo Class
 *
 * Add Seo features into CodeIgniter projects.
 *
 * @license     MIT License
 * @package     Codeigniter
 * @subpackage  Libraries
 * @category    Library
 * @author      Nilton Freitas
 * @link        http://niltonfreitas.com/codeigniter-seo-library
 * @version     0.0.1
 * @todo        Docs
 */

Class Seo
{
    //General Site SEO info
    private $author;
    private $description;
    private $keywords;
    private $name;
    private $title;


    function __construct($dados)
    {
        $this->ci =& get_instance();
        $this->ci->load->config('seo', TRUE);
        $this->name = $this->ci->config->item('site_name', 'seo');
        $this->author = $this->ci->config->item('site_author', 'seo');
        $this->keywords = (isset($dados['keywords'])) ? $dados['keywords'] 
                        : $this->ci->config->item('site_keywords', 'seo');
        $this->title = (isset($dados['title'])) ? $dados['title'] 
                        : $this->ci->config->item('site_title', 'seo');
        $this->description = (isset($dados['description'])) ? $dados['description']
                        : $this->ci->config->item('site_description', 'seo');
    }
    /**
     *
     * @param  string $given [description]
     * @return [type]        [description]
     */
    function get_title()
    {
        echo $this->name . ' &middot; ' . $this->title;
    }

    function site_name()
    {
        echo $this->name;
    }

    public function build_meta()
    {
        $meta = array();
        if(!is_null($this->description))
        {
            $meta['description']   = '<meta name="description" content="';
            $meta['description']  .= $this->description;
            $meta['description']  .= '">';
        }

        if(!is_null($this->keywords))
        {
            $meta['keywords']   = '        <meta name="keywords" content="';
            $meta['keywords']  .= $this->keywords;
            $meta['keywords']  .= '">';
        }

        if(!is_null($this->author))
        {
            $meta['author']   = '        <meta name="author" content="';
            $meta['author']  .= $this->author;
            $meta['author']  .= '">';
        }

        foreach($meta as $key => $value)
        {
            echo $value . "\r\n";
        }
    }
}

/* End of file seo.php */
/* Location: ./application/libraries/seo.php */
