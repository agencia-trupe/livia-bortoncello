<?php echo Modules::run('contato/parcial') ?>
<div class="sidebar">
    <div class="marca-contato">
        <div class="marca-wrapper">
            <div class="clearfix"></div>
            <a href="<?php echo site_url() ?>" class="marca">Livia Bortoncello - Home</a>
        </div><!--.marca-wrapper-->
    </div><!--.marca-contato-->
    <div class="menu">
        <?php echo Modules::run('nav/menu', $pagina); ?>
    </div>
</div>