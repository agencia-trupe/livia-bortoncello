<?php
class Mostra extends Datamapper
{
    var $table = 'mostras';
    var $has_many = array('fotomostra');
    var $auto_populate_has_many = TRUE;

    function get_all()
    {
        $mostra = new Mostra();
        $mostra->order_by( 'ordem', 'ASC' )->get();
        $arr = array();
        foreach( $mostra->all as $mostra )
        {
            $arr[] = $mostra;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo( $id )
    {
        $mostra = new Mostra();
        $mostra->where( 'id', $id )->get();
        if( ! $mostra->exists() ) NULL;
        return $mostra;
    }

    function get_related( $ordem, $position )
    {
        $mostra = new Mostra();
        switch ( $position ) {
            case 'prev':
                $mostra->order_by('ordem', 'DESC');
                $mostra->where( 'ordem <', $ordem );
                break;
            case 'next':
                $mostra->order_by('ordem', 'ASC');
                $mostra->where( 'ordem >', $ordem );
                break;
        }

        $mostra->get(1);
        if( ! $mostra->exists() ) return FALSE;
        
        return $mostra->id;
    }

    function insert($dados)
    {
        $mostras = new Mostra();
        $mostras->get();

        $count = $mostras->result_count();

        $mostra = new Mostra();
        foreach ($dados as $key => $value)
        {
            $mostra->$key = $value;
        }
        $mostra->ordem = $mostras->result_count();
        $mostra->created = time();
        $insert = $mostra->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

        function change($dados)
    {
        $mostra = new Mostra();
        $mostra->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $mostra->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $foto = new Mostra();
        $foto->where('id', $id)->get();
        if($foto->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $categoria = new Mostra();
            $categoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($categoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}