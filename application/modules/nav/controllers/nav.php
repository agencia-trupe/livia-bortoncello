<?php

class Nav extends MX_Controller
{
	public function menu($pagina)
	{
		$this->load->model('projetos/projeto');
		$this->load->model('projetos/categoria');
		$this->load->model('projetos/subcategoria');
		$this->load->model('contato/endereco');

        $data['contato'] = $this->endereco->get_info();

		$data['pagina'] = $pagina;
		$this->load->view('nav/menu', $data);
	}
}