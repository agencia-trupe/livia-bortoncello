<?php
class Admin_projetos extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'projetos';
        $this->load->model('projetos/projeto');
        $this->load->model('projetos/foto');
        $this->load->model('projetos/categoria');
        $this->load->model('projetos/subcategoria');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista($subcategoria_id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['subcategoria_id'] = $subcategoria_id;
            $this->data['projetos'] = $this->projeto->get_all($subcategoria_id, 'subcategoria_id');
            $this->data['conteudo'] = 'projetos/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['categorias'] = $this->categoria->get_all();
            $this->data['subcategorias'] = $this->subcategoria->get_all();
            $this->data['projeto'] = $this->projeto->get_conteudo($id, 'id');
            $this->data['subcategoria_id'] = $this->data['projeto']->subcategoria_id;
            $this->data['categoria_id'] = $this->data['projeto']->categoria_id;
            $this->data['fotos'] = $this->foto->get_projeto($id);
            $this->data['conteudo'] = 'projetos/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar($subcategoria_id = NULL)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['subcategoria_id'] = $subcategoria_id;
            if($subcategoria_id)
            {
                $subcategoria = $this->subcategoria->get_conteudo($subcategoria_id, 'id');
                $this->data['categoria_id'] = $subcategoria->categoria_id;
            }
            $this->data['categorias'] = $this->categoria->get_all();
            $this->data['subcategorias'] = $this->subcategoria->get_all();
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'projetos/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('projetos'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'projetos/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/projetos/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( ! $this->upload->do_upload('imagem'))
                    {

                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['projeto'] = $this->projeto->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'projetos/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        //Locais dos arquivos alterados
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];
                        $capa = $upload_data['file_path'] . './capas/' . $upload_data['file_name'];

                        if(
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize(660,440,FALSE)
                                 ->save($new_file,true)
                            &&
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(50,50)
                                 ->save($thumb,true)
                            &&
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(190,190)
                                 ->save($capa,true)
                        )
                        {
                            $post['capa'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['capa'] = NULL;
                        }
                    }
                }
                $post['user_id'] = $this->tank_auth->get_user_id();
                if($this->projeto->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/projetos/lista/' . $post['subcategoria_id']);
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/projetos/lista/' . $post['subcategoria_id']);
                }
            }
        }
    }

    public function sort_fotos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('foto');
            if ($itens)
            {
                $ordenar = $this->foto->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }

    public function deleta_projeto($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $projeto = $this->projeto->get_conteudo($id);
            $apaga = $this->projeto->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/projetos/lista/' . $projeto->subcategoria_id);
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/projetos/lista/' . $projeto->subcategoria_id);
            }
        }
    }

    public function deleta_foto()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if($this->input->post('ajax') == 1)
            $deleta = $this->foto->deleta_foto($this->input->post('foto_id'));
            if($deleta)
            {
                $response['status'] = 'success';
                $response['msg'] = 'Foto apagada com sucesso.';
            }
            else
            {
                $response['status'] = 'error';
                $response['msg'] = 'Erro ao apagar, por favor, 
                tente novamente';
            }
            echo json_encode($response);
        }
    }

    public function adiciona_foto()
    { 
        $this->load->library('form_validation');

        $status = "";
        $msg = "";
        $file_element_name = 'projeto-foto-upload';
   
        $config['upload_path'] = './assets/img/projetos/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']  = 1024 * 8;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $id = '';
            $nome = '';
        }
        else
        {
            $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
            $upload_data = $this->upload->data();
            $file_uploaded = $upload_data['full_path'];
            //Locais dos arquivos alterados
            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
            $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];

            if(
                $this->image_moo
                     ->load($file_uploaded)
                     ->resize(660,440,FALSE)
                     ->save($new_file,true)
                &&
                $this->image_moo
                     ->load($file_uploaded)
                     ->resize_crop(50,50)
                     ->save($thumb,true)
            )
            {
                $imagem = $upload_data['file_name'];
                    $data = array(
                    'imagem' => $imagem,
                    'projeto_id' => $this->input->post('projeto_id'),
                    'titulo' => 'Teste'
                    );
                $adiciona = $this->foto->insert($data);
                if($adiciona)
                {
                    $status = "success";
                    $msg = "Foto adicionada com sucesso.";
                    $id = $adiciona;
                    $nome = $data['imagem'];
                }
                else
                {
                    unlink($data['file_data']['full_path']);
                    $status = "error";
                    $msg = "Houve um erro ao processar sua requisição. Por vavor, tente novamente, ou entre em contato com o suporte.";
                    $id = '';
                    $nome = '';
                }
            }
            else
            {
                $status = 'error';
                $msg = $this->image_moo->display_errors();
                $id = '';
                $nome = '';
            }
        }   
        @unlink($_FILES[$file_element_name]);
        echo json_encode(array('status' => $status, 'msg' => $msg, 'id' => $id, 'nome' => $nome));
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('projetos'))
            {
                $this->data['acao'] = 'editar';
                $this->data['projeto'] = $this->projeto->get_conteudo($this->input->post('id'));
                $this->data['fotos'] = $this->foto->get_projeto($this->input->post('id'));
                $this->data['conteudo'] = 'projetos/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {
                    
                    $config['upload_path'] = './assets/img/projetos/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '8000';
                    $config['max_width']  = '6000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( !$this->upload->do_upload('imagem'))
                    {

                            $this->data['acao'] = 'editar';
                            $this->data['projeto'] = $this->projeto->get_conteudo($this->input->post('id'));
                            $this->data['fotos'] = $this->foto->get_projeto($this->input->post('id'));
                            $this->data['conteudo'] = 'teste';
                            $this->load->view('teste', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        //Locais dos arquivos alterados
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . './thumbs/' . $upload_data['file_name'];
                        $capa = $upload_data['file_path'] . './capas/' . $upload_data['file_name'];

                        if(
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize(660,440,FALSE)
                                 ->save($new_file,true)
                            &&
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(50,50)
                                 ->save($thumb,true)
                            &&
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(190,190)
                                 ->save($capa,true)
                        )
                        {
                            $post['capa'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['capa'] = NULL;
                        }
                    }
                }
                $post['updated'] = time();
                if($this->projeto->change($post))
                {
                    log_message('info', 'Cadastro alterado');
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/projetos/lista/' . $this->input->post('subcategoria_id'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/projetos/edita/' . $post['id']);
                }
            }
        }
    }

    /**
     * Reordena os subcategorias de projetos para a exibição
     * @return void status do processamento
     */
    public function sort_projetos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('projeto');
            if ($itens)
            {
                $ordenar = $this->projeto->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}