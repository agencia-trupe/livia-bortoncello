<?php
class Admin_categorias extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'projetos';
        $this->load->model('projetos/categoria');
        $this->load->model('projetos/subcategoria');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['categorias'] = $this->categoria->get_all();
            $this->data['conteudo'] = 'projetos/admin_lista_categorias';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['categoria'] = $this->categoria->get_conteudo($id, 'id');
            $this->data['conteudo'] = 'projetos/admin_edita_categorias';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'projetos/admin_edita_categorias';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('categorias'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'projetos/admin_edita_categorias';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                //Obtém o slug
                $this->load->helper('blog');
                $post['slug'] = get_slug($post['titulo']);

                //Id do usuário
                $post['user_id'] = $this->tank_auth->get_user_id();

                if($this->categoria->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/projetos/categorias');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/projetos/categorias/');
                }
            }
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('categorias'))
            {
                $this->data['acao'] = 'editar';
                $this->data['categoria'] = $this->categoria->get_conteudo($this->input->post('id'));
                $this->data['conteudo'] = 'projetos/admin_edita_categorias';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }

                //Obtém o slug
                $this->load->helper('blog');
                $post['slug'] = get_slug($post['titulo']);

                //Id do usuário
                $post['user_id'] = $this->tank_auth->get_user_id();

                if($this->categoria->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/projetos/categorias');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/projetos/categorias/edita/' . $post['id']);
                }
            }
        }
    }

    public function deleta_categoria($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->categoria->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/projetos/categorias');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/projetos/categorias/');
            }
        }
    }

    /**
     * Reordena os categorias de produtos para a exibição
     * @return void status do processamento
     */
    public function sort_categorias()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('categoria');
            if ($itens)
            {
                $ordenar = $this->categoria->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}