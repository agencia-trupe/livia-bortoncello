<div class="conteudo-projetos projetos-detalhe">
    <div class="interna">
        <h1><?php echo $projeto->titulo ?></h1>
        <div class="projetos-detalhe-wrapper">
            <div id="projetos-detalhe-tabs">
                <div class="projetos-detalhe-ampliada">
                    <div class="projetos-slide">
                        <div class="projetos-image-wrapper">
                            <img src="<?php echo base_url('assets/img/projetos/' . $projeto->capa ) ?>" alt="<?php echo $projeto->titulo ?>">                        
                            <div class="projetos-slide-control">
                                <a href="#" class="projetos-slide-prev"></a>
                                <a href="#" class="projetos-slide-next"></a>
                            </div>
                        </div>
                    </div>
                    <?php if($fotos): ?>
                    <?php foreach ($fotos as $foto): ?>
                        <div class="projetos-slide">
                            <div class="projetos-image-wrapper">
                                <img src="<?php echo base_url('assets/img/projetos/' . $foto->imagem ) ?>" alt="<?php echo $projeto->titulo ?>">
                                <div class="projetos-slide-control">
                                    <a href="#" class="projetos-slide-prev"></a>
                                    <a href="#" class="projetos-slide-next"></a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                    <?php endif; ?>
                </div>
                <div class="data-descricao">
                    <p class="data"><?php echo $projeto->data ?></p>
                    <p class="descricao"><?php echo $projeto->descricao ?></p>
                </div>
                <div class="controle-social">
                    <div class="projetos-detalhe-social">
                        <iframe src="//www.facebook.com/plugins/like.php?href=<?php echo current_url() ?>&amp;send=false&amp;layout=standard&amp;width=65&amp;show_faces=false&amp;font&amp;colorscheme=light&amp;action=like&amp;height=35&amp;appId=355115464591773" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:65px; height:24px;" allowTransparency="true"></iframe>
                    </div>
                </div>
                <a href="<?php echo site_url('projetos/' . $categoria . '/' . $subcategoria) ?>" class="voltar"><span>&laquo;</span>voltar</a>
                <div class="clearfix"></div>
                <div id="bx-pager">
                                <a data-slide-index="0" href="#">
                                    <img src="<?php echo base_url('assets/img/projetos/thumbs/' . $projeto->capa) ?>" alt="<?php echo $projeto->titulo ?>">  
                                </a>
                            <?php if($fotos): ?>
                            <?php for ($i = 0; $i <  $count; $i++): ?>
                                <a data-slide-index="<?php echo $i + 1 ?>" class="" href="#">
                                    <img src="<?php echo base_url('assets/img/projetos/thumbs/' . $fotos[$i]->imagem) ?>" alt="<?php echo $projeto->titulo ?>">  
                                </a>
                            <?php endfor ?>
                            <?php endif; ?>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>