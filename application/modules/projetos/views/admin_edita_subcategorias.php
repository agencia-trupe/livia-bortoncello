<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
        <div class="alert alert-error">
            <?php echo $this->session->flashdata('error'); ?>
        </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
        <div class="alert alert-success">
            <?php echo $this->session->flashdata('success'); ?>
        </div>
    <?php endif; ?>
    <?php if(isset($error)): ?>
        <div class="alert alert-error">
            <?php echo $error['error']; ?>
        </div>
    <?php endif; ?>
    <legend><?=( $acao == 'editar' ) ? 'Editar' : 'Cadastrar'; ?> Subcategoria de Projetos</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/projetos/categorias/subcategorias/processa';
                    break;
                
                default:
                    $action = 'painel/projetos/categorias/subcategorias/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
    <input type="hidden" name="id" value="<?=$subcategoria->id; ?>" class="id" />
    <?php endif; ?>
    <?=form_label('Título'); ?>
    <?=form_input(array(
        'name' => 'titulo',
        'value' => set_value('titulo', ( $acao == 'editar' ) ? $subcategoria->titulo : '')
    )); ?>
    <?=form_error('titulo'); ?>

    <?=form_label('Categoria'); ?>
    <select name="categoria_id" id="categoria_id">
        <?php foreach ($categorias as $categoria): ?>
            <option value="<?php echo $categoria->id ?>" <?php echo ( $categoria_id == $categoria->id ) ? 'selected' : '' ?>><?php echo $categoria->titulo ?></option>
        <?php endforeach ?>
    </select>
    
    <div class="clearfix"></div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('projetos/admin_subcategorias/lista', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    </div>
</div>