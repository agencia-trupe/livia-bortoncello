<div class="row-fluid">
    <div class="span12">
            <legend>Subcategorias de Projeto 
                <?php echo anchor('painel/projetos/cadastrar', 'novo projeto', 'class="btn btn-info btn-mini"'); ?> 
                <?php echo anchor('painel/projetos/categorias/subcategorias/cadastrar/' . $categoria_id, 'Nova subcategoria', 'class="btn btn-info btn-mini"'); ?>  
                <a href="#" class="ordenar-subcategorias btn btn-mini btn-info">ordenar subcategorias</a>
                <a href="#" class="salvar-ordem-subcategorias hide btn btn-mini btn-warning">salvar ordem</a>
                <?php echo anchor('painel/projetos/categorias/', 'voltar', 'class="btn btn-danger btn-mini"'); ?>  
            </legend>
            <div class="alert alert-info hide subcategorias-mensagem">
                <span>Para ordenar, clique no nome da subcategoria e arraste até a posição desejada</span>
                <a class="close" data-dismiss="alert" href="#">&times;</a>
            </div>

     <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Ordem</th>
                <th class="span6">Título</th>
                <th>Criação</th>
                <th>Atualização</th>
                <th>Usuário</th>
                <th class="span5"><i class="icon-cog"></i></th>
            </tr>
        </thead>
        <tbody>
            <?php if($subcategorias): ?>
            <?php foreach ($subcategorias as $subcategoria): ?>
                <tr id="subcategoria_<?php echo $subcategoria->id ?>">
                    <td><?=$subcategoria->ordem; ?></td>
                    <td><?=$subcategoria->titulo; ?></td>
                    <td><?=date('d/m/Y', $subcategoria->created); ?></td>
                    <td>
                        <?php if($subcategoria->updated): ?>
                            <?=date('d/m/Y', $subcategoria->updated); ?>
                        <?php endif; ?>
                    </td>
                    <td><?=$this->tank_auth->get_username($subcategoria->user_id); ?></td>
                    <td><?=anchor('painel/projetos/lista/' . $subcategoria->id, '<i class="icon-th-list icon-white"></i> projetos', 'class="btn btn-mini btn-info"'); ?>
                        <?=anchor('painel/projetos/categorias/subcategorias/editar/' . $subcategoria->id, '<i class="icon-pencil icon-white"></i> editar', 'class="btn btn-mini btn-warning"'); ?>
                        <?=anchor('painel/projetos/categorias/subcategorias/deleta_subcategoria/' . $subcategoria->id, '<i class="icon-remove-circle icon-white" ></i> remover', 'class="btn btn-mini btn-danger"'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
            <?php endif; ?>
        </tbody>
    </table>
    </div>
</div>