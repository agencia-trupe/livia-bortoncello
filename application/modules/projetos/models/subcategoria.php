<?php
class Subcategoria extends Datamapper
{
    var $table = 'subcategorias';
    var $has_one = array('categoria');

    function get_all(  $categoria_id = NULL )
    {
        $subcategoria = new Subcategoria();
        if($categoria_id)
        {
            $subcategoria->where('categoria_id', $categoria_id);
        }
        $subcategoria->order_by( 'ordem', 'ASC' )->get();
        $arr = array();
        foreach( $subcategoria->all as $subcategoria )
        {
            $arr[] = $subcategoria;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return FALSE;
    }
    function get_conteudo( $value, $criteria )
    {
        $subcategoria = new Subcategoria();
        $subcategoria->where( $criteria, $value )->get();
        if( ! $subcategoria->exists() ) NULL;
        return $subcategoria;
    }

    function insert($dados)
    {
        $subcategorias = new Subcategoria();
        $subcategorias->where('categoria_id', $dados['categoria_id']);
        $subcategorias->get();

        $count = $subcategorias->result_count();

        $subcategoria = new Subcategoria();
        foreach ($dados as $key => $value)
        {
            $subcategoria->$key = $value;
        }
        $subcategoria->ordem = $count;
        $subcategoria->created = time();

        $insert = $subcategoria->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change($dados)
    {
        $subcategoria = new Subcategoria();
        $subcategoria->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        
        $update = $subcategoria->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $subcategoria = new Subcategoria();
        $subcategoria->where('id', $id)->get();
        if($subcategoria->delete())
        {
            return  TRUE;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $subcategoria = new Subcategoria();
            $subcategoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($subcategoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}