<div id="direita">
    <div class="row-fluid inscreve">
    <div class="alert alert-error span11">
        <h4 class="alert-heading">
            Erro
        </h4>
        <p>
            Houve um erro ao processar sua solicitação, <a href="<?php echo base_url(); ?>">clique aqui</a> e volte para
            a página inicial.
            <br><br>
            Se preferir, você pode informar nosso suporte sobre este erro através
            do email <a href="mailto:suporte@institutointeragir.org.br">
                suporte@institutointeragir.org.br</a> informando o código <?php echo $codigo; ?>.
        </p>
    </div>
    </div>
</div>