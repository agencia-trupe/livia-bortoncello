<div class="span9">
  <h1>Registros do tipo <?php echo $tipo; ?></h1>
<?php
        
            $tmpl = array (
                                'table_open'          => '<table class="table table-striped table-bordered table-condensed">',

                                'heading_row_start'   => '<tr>',
                                'heading_row_end'     => '</tr>',
                                'heading_cell_start'  => '<th>',
                                'heading_cell_end'    => '</th>',

                                'row_start'           => '<tr>',
                                'row_end'             => '</tr>',
                                'cell_start'          => '<td>',
                                'cell_end'            => '</td>',


                                'table_close'         => '</table>'
                        );

            $this->table->set_template($tmpl); 
            $this->table->set_heading(array('Usuário', 'Ip' ,'Data', 'Mensagem'));
            foreach ($result as $item)
            {  
            $mensagem = $item->mensagem;
            $usuario = $item->perfil->nome;
            $ip = $item->ip;
            
            
            $this->table->add_row(array($usuario, $ip, date('d/m/Y - h:i:s A', $item->time), $mensagem));
            }
            echo $this->table->generate();

            ?> 
                <?php echo $this->pagination->create_links(); ?>
        

</div>