<?php

class Logs extends MX_Controller
{
    public function registra($tipo = NULL, $user_id = NULL, $message = NULL)
    {
        if($user_id == NULL)
        {
            $user_id = $this->tank_auth->get_user_id();
        }
        $ip = $_SERVER['REMOTE_ADDR'];
        $this->load->model('logger');
        $this->logger->salva_log($tipo, $user_id, $ip, $message);
    }
}