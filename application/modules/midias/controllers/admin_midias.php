<?php
class Admin_midias extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'midia';
        $this->load->model('midias/midia');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['midias'] = $this->midia->get_all();
            $this->data['conteudo'] = 'midias/admin_lista';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->load->model('midias/clipping');
            $this->data['midia'] = $this->midia->get_conteudo($id, 'id');
            $this->data['clipping'] = $this->clipping->get_clipping($this->data['midia']->id);
            $this->data['acao'] = 'editar';
            $this->data['conteudo'] = 'midias/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'midias/admin_edita';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('midia'))
            {
                $this->data['midia'] = $this->midia->get_conteudo($this->input->post( 'id' ), 'id');
                $this->data['conteudo'] = 'midias/admin_edita';
                $this->data['acao'] = 'editar';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {

                    $config['upload_path'] = './assets/img/clipping/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '4000';
                    $config['max_width']  = '4000';
                    $config['max_height']  = '4000';

                    $this->load->library('upload', $config);

                    if ( !$this->upload->do_upload('imagem'))
                    {
                            $data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['midia'] = $this->midia->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'midias/admin_edita';
                            $this->data['acao'] = 'editar';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(492,642)
                                 ->save($new_file,true)
                            &&
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(180,240)
                                 ->save($thumb,true)
                            )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['imagem'] = NULL;
                        }
                    }
                }
                $post['updated'] = time();
                if($this->midia->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/midia');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/midia/edita/' . $post['id']);
                }
            }
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('midia'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'midias/admin_edita';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if(strlen($_FILES["imagem"]["name"])>0)
                {

                    $config['upload_path'] = './assets/img/clipping/';
                    $config['allowed_types'] = 'gif|jpg|png';
                    $config['max_size'] = '4000';
                    $config['max_width']  = '1600';
                    $config['max_height']  = '1200';

                    $this->load->library('upload', $config);

                    if ( !$this->upload->do_upload('imagem'))
                    {
                            $this->data['acao'] = 'cadastrar';
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                            $this->data['midia'] = $this->midia->get_conteudo($id, $this->input->post('id'));
                            $this->data['conteudo'] = 'midias/admin_edita';
                            $this->load->view('start/template', $this->data);
                    }
                    else
                    {
                        $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
                        $upload_data = $this->upload->data();
                        $file_uploaded = $upload_data['full_path'];
                        $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
                        $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];

                        if(
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(492,642)
                                 ->save($new_file,true)
                            &&
                            $this->image_moo
                                 ->load($file_uploaded)
                                 ->resize_crop(180,240)
                                 ->save($thumb,true)
                            )
                        {
                            $post['imagem'] = $upload_data['file_name'];
                        }
                        else
                        {
                            $post['imagem'] = NULL;
                        }
                    }
                }
                $post['updated'] = time();
                if(is_null($post['imagem']))
                {
                    $this->session->set_flashdata('error', 'Você precisa selecionar uma imagem.');
                    redirect('painel/midia/cadastrar');
                }
                elseif($this->midia->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/midia');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/midia/edita/cadastrar');
                }
            }
        }
    }

    public function deleta_midia($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->midia->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/midia');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/midia/');
            }
        }
    }

    public function adiciona_foto()
    {
        $this->load->model('midias/clipping');

        $this->load->library('form_validation');

        $status = "";
        $msg = "";
        $file_element_name = 'projeto-foto-upload';
   
        $config['upload_path'] = './assets/img/clipping/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']  = 1024 * 8;
        $config['encrypt_name'] = FALSE;
        $this->load->library('upload', $config);
        if (!$this->upload->do_upload($file_element_name))
        {
            $status = 'error';
            $msg = $this->upload->display_errors('', '');
            $id = '';
            $nome = '';
        }
        else
        {
            $this->load->library('image_moo');
                        //Is only one file uploaded so it ok to use it with $uploader_response[0].
            $upload_data = $this->upload->data();
            $file_uploaded = $upload_data['full_path'];
            $new_file = $upload_data['file_path'] . './' . $upload_data['file_name'];
            $thumb = $upload_data['file_path'] . 'thumbs/' . $upload_data['file_name'];

            if(
                $this->image_moo->load($file_uploaded)
                                ->resize_crop(492,642)
                                ->save($new_file,true)
                &&
                $this->image_moo->load($file_uploaded)
                                ->resize_crop(180,240)
                                ->save($thumb, true)
              )
            {
                $imagem = $upload_data['file_name'];
                    $data = array(
                    'imagem' => $imagem,
                    'midia_id' => $this->input->post('midia_id'),
                    'titulo' => 'Teste'
                    );
                $adiciona = $this->clipping->insert($data);
                if($adiciona)
                {
                    $status = "success";
                    $msg = "Foto adicionada com sucesso.";
                    $id = $adiciona;
                    $nome = $data['imagem'];
                }
                else
                {
                    unlink($data['file_data']['full_path']);
                    $status = "error";
                    $msg = "Houve um erro ao processar sua requisição. Por vavor, tente novamente, ou entre em contato com o suporte.";
                    $id = '';
                    $nome = '';
                }
            }
            else
            {
                $status = 'error';
                $msg = $this->image_moo->display_errors();
                $id = '';
                $nome = '';
            }
        }   
        @unlink($_FILES[$file_element_name]);
        echo json_encode(array('status' => $status, 'msg' => $msg, 'id' => $id, 'nome' => $nome));
    }

    public function deleta_foto()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->load->model('midias/clipping');
            
            if($this->input->post('ajax') == 1)
            $deleta = $this->clipping->deleta_foto($this->input->post('foto_id'));
            if($deleta)
            {
                $response['status'] = 'success';
                $response['msg'] = 'Foto apagada com sucesso.';
            }
            else
            {
                $response['status'] = 'error';
                $response['msg'] = 'Erro ao apagar, por favor, 
                tente novamente';
            }
            echo json_encode($response);
        }
    }

    /**
     * Reordena os tipos de projetos para a exibição
     * @return void status do processamento
     */
    public function sort_midias()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Livia Bortoncello - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('midia');
            if ($itens)
            {
                $ordenar = $this->midia->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}