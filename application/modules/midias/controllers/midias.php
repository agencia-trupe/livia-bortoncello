<?php
class Midias extends MX_Controller
{
    var $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('midias/midia');
        $this->load->model('midias/clipping');
        $this->data['pagina'] = 'midia';
    }
    public function index()
    {
        $this->lista();
    }
    public function lista()
    {
        $result = $this->midia->get_all();
        $midias = array();

        foreach ($result as $midia) {
            
            
            $clipping_result = $this->clipping->get_midia_clipping($midia->id);
            $fotos = array();
            if($clipping_result)
            {
                 foreach($clipping_result as $foto)
                {
                    $fotos[]['imagem'] = $foto->imagem;
                }
            }
           

            $midias[$midia->id]['imagem'] = $midia->imagem;
            $midias[$midia->id]['titulo'] = $midia->titulo;
            if(isset($fotos[0]))
                $midias[$midia->id]['foto2'] = $fotos[0];
            unset($fotos[0]);
            $midias[$midia->id]['fotos'] = array_chunk($fotos, 2);
        }

        $this->data['midias'] = $midias;
        $this->data['conteudo'] = 'midias/index';
        $seo = array(
            'titulo' => 'Mídia',
            'descricao' =>  'Livia Bortoncello Arquitetura na Mídia' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }

    public function detalhe($id)
    {
        $this->load->model('midias/clipping');
        $this->load->model('midias/midia');

        $this->data['clipping'] = $this->clipping->get_clipping($id);
        $this->data['conteudo'] = 'midias/detalhe';

        $this->data['prev'] = $this->midia->get_prev($id);
        $this->data['next'] = $this->midia->get_next($id);

        $seo = array(
            'titulo' => 'Mídia',
            'descricao' =>  'Livia Bortoncello Arquitetura na Mídia' 
            );
        $this->load->library( 'seo', $seo );
        $this->load->view( 'layout/template', $this->data );
    }
}