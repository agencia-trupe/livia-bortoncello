<div class="conteudo-clipping">
    <div class="interna">
        <div class="clipping-wrapper">
            <?php foreach ( $midias as $id => $content ): ?>
            <a href="javascript:void(0)" onclick = "document.getElementById('light<?php echo $id ?>').style.display='block';document.getElementById('fade').style.display='block'" class="clipping">
                <img src="<?php echo base_url('assets/img/clipping/thumbs/' . $content['imagem']) ?>" alt="">
                <span class="clipping-img-hover"></span>
                <span class="clipping-titulo-wrapper">
                    <span class="clipping-titulo">
                        <?php echo $content['titulo'] ?>
                    </span>
                </span>
                <span class="clearfix"></span>
            </a>
            <?php endforeach ?>
            <?php foreach ($midias as $id => $content): ?>
                <?php if(!count($content['fotos'])): ?>
                <div id="light<?php echo $id ?>" class="white_content">
                    <a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $id ?>').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
                    <div class="clearfix"></div>
                    <div class="clipping-slider-wrapper">
                        <div class="clipping-slide">
                            <div class="">
                                <div class="image-left <?php echo ( ! isset($content['foto2']) ) ? 'image-center' : '' ?>">
                                    <img src="<?php echo base_url('assets/img/clipping/' . $content['imagem']) ?>" alt="">
                                </div>
                                <?php if(isset($content['foto2'])): ?>
                                    <div class="image-left">
                                        <img src="<?php echo base_url('assets/img/clipping/' . $content['foto2']['imagem']) ?>" alt="">
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php elseif( count($content['fotos']) && count($content['fotos'][0]) == 1): ?>
                     <div id="light<?php echo $id ?>" class="white_content">
                        <a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $id ?>').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
                        <div class="clearfix"></div>
                        <div class="clipping-slider-wrapper">
                            <div class="clipping-slider">
                                <div class="clipping-slide">
                                    <div class="">
                                        <div class="image-left">
                                            <img src="<?php echo base_url('assets/img/clipping/' . $content['imagem']) ?>" alt="">
                                        </div>
                                        <div class="image-left">
                                            <img src="<?php echo base_url('assets/img/clipping/' . $content['foto2']['imagem']) ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="clipping-slide">
                                    <div class="">
                                        <div class="image-left">
                                            <img src="<?php echo base_url('assets/img/clipping/' . $content['fotos'][0][0]['imagem']) ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php elseif( count($content['fotos']) && count($content['fotos'][0]) > 1): ?>
                     <div id="light<?php echo $id ?>" class="white_content">
                        <a class="fechar-clipping" href = "javascript:void(0)" onclick = "document.getElementById('light<?php echo $id ?>').style.display='none';document.getElementById('fade').style.display='none'">Close</a>
                        <div class="clearfix"></div>
                        <div class="clipping-slider-wrapper">
                            <div class="clipping-slider">
                                 <div class="clipping-slide">
                                    <div class="">
                                        <div class="image-left">
                                            <img src="<?php echo base_url('assets/img/clipping/' . $content['imagem']) ?>" alt="">
                                        </div>
                                        <div class="image-left">
                                            <img src="<?php echo base_url('assets/img/clipping/' . $content['foto2']['imagem']) ?>" alt="">
                                        </div>
                                    </div>
                                </div>
                                <?php foreach ($content['fotos'] as $fotos) : ?>
                                <div class="clipping-slide">
                                    <div class="">
                                        <?php foreach($fotos as $foto): ?>
                                        <div class="image-left">
                                            <img src="<?php echo base_url('assets/img/clipping/' . $foto['imagem']) ?>" alt="">
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endforeach ?>
            <div id="fade" class="black_overlay" ></div>
        </div>
    </div> 
</div>
<div class="clearfix"></div>