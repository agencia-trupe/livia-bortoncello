<div class="conteudo midias detalhe">
    <ul class="clipping">
        <?php foreach ($clipping as $midia): ?>
            <li><img src="<?php echo base_url('assets/img/midia/fotos/' . $midia->imagem) ?>" alt="<?php echo $midia->titulo ?>"></li>
        <?php endforeach ?>
    </ul>
	<div id="navigation">
		<a href="#" class="scrollToTop">voltar ao topo</a>
		<div class="next-prev">
			<a href="<?php echo site_url('midia/detalhe/' . $prev) ?>" class="prev">próximo</a>
			<a href="<?php echo site_url('midia/detalhe/' . $next) ?>" class="next">anterior</a>
		</div>
	</div>
	<div class="clearfix"></div>
</div>