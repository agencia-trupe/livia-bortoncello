<?php
class Clipping extends Datamapper
{
    var $table = 'clipping';

    function get_all()
    {
        $midia = new Midia();
        $midia->get();
        $arr = array();
        foreach( $midia->all as $midia )
        {
            $arr[] = $midia;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return NULL;
    }

    function get_midia_clipping($midia_id)
    {
        $midia = new Clipping();
        $midia->where( 'midia_id', $midia_id )->get();
        $arr = array();
        foreach( $midia->all as $midia )
        {
            $arr[] = $midia;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return NULL;
    }

    



    function get_clipping( $midia_id )
    {
        $clipping = new Clipping();
        $clipping->where( 'midia_id', $midia_id )->get();
        if( $clipping->exists() ){
            return $clipping;
        }
        return NULL;
    }

    function get_related( $id, $position )
    {
        $midia = new Midia();
        switch ( $position ) {
            case 'prev':
                $midia->where( 'id <', $id);
                break;
            case 'next':
                $midia->where( 'id >', $id);
                break;
        }
        $midia->get(1);
        if($midia->exists())
        {
            return $midia->id;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $clipping = new Clipping();
        foreach ($dados as $key => $value)
        {
            $clipping->$key = $value;
        }
        $clipping->created = time();
        $insert = $clipping->save();
        if($insert)
        {
            return $clipping->id;
        }
        return FALSE;
    }

    function change($dados)
    {
        $midia = new Midia();
        $midia->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $midia->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $servico = new Midia();
        $servico->where('id', $id)->get();
        if($servico->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    function deleta_foto($foto_id)
    {
        $foto = new Clipping();
        $foto->where('id', $foto_id)->get();
        if($foto->delete())
        {
            return TRUE;
        }
        return FALSE;
    }
}