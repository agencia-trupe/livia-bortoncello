<div class="row-fluid">
    <div class="span9">
        <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <legend><?=($acao == 'editar') ? 'Editar' : 'Cadastrar'; ?> Colaborador</legend>
    <?php 
            switch ($acao) {
                case 'editar':
                    $action = 'painel/colaboradores/processa';
                    break;
                
                default:
                    $action = 'painel/colaboradores/processa_cadastro';
                    break;
            }
    ?>
    <?=form_open_multipart($action); ?>
    <?php if($acao == 'editar'): ?>
        <input type="hidden" name="id" value="<?php echo $colaborador->id ?>" class="id" >
    <?php endif; ?>
    <div class="control-group">
        <label class="control-label" for="nome">Nome</label>
        <div class="controls">
            <?php echo form_input('nome', set_value('nome', ( $acao == 'editar' ) ? $colaborador->nome : '' )); ?>
            <span class="help-inline"><?php echo form_error('nome'); ?></span>
        </div>
    </div>

    <div class="control-group">
        <label class="control-label" for="texto">Conteúdo</label>
        <div class="controls">
            <?php echo form_textarea(array('name' => 'texto', 'value' => set_value('texto', ( $acao == 'editar') ? $colaborador->texto : '' ), 'class' => 'tinymce')); ?>
            <span class="help-inline"><?php echo form_error('texto'); ?></span>
        </div>
    </div>
    
    <?php if($acao == 'editar' && $colaborador->imagem):?>
    <img src="<?php echo base_url(); ?>assets/img/colaboradores/thumbs/<?php echo $colaborador->imagem; ?>" alt="" >
    <?php endif; ?>
    <br>
    <div class="control-group">
           <label class="control-label" for="imagem"><?php echo ( $acao == 'editar' ) ? 'Alterar Imagem' : 'Adicionar Imagem'; ?></label>
           <div class="controls">
             <?php echo form_upload('imagem', set_value('imagem')); ?>
             <span class="help-inline"><?php echo form_error('imagem'); ?></span>
           </div>
    </div>
    <?=form_submit('', 'Salvar' , 'class="btn btn-info"'); ?>
    <?=anchor('painel/colaboradores', 'Cancelar', 'class="btn btn-warning"'); ?>
    <?=form_close(); ?>
    <div class="clearfix"></div>
    </div>
</div>