<?php
class Colaborador extends Datamapper
{
    var $table = 'colaboradores';

    function get_all()
    {
        $colaborador = new Colaborador();
        $colaborador->order_by('nome', 'ASC')->get();
        $arr = array();
        foreach($colaborador->all as $colaborador)
        {
            $arr[] = $colaborador;
        }
        if(sizeof($arr))
        {
            return $arr;
        }
        return FALSE;
    }

    function get_conteudo($id)
    {
        $colaborador = new Colaborador();
        $colaborador->where('id', $id)->get();

        if( $colaborador->exists() )
        {
            return $colaborador;
        }
        return NULL;
    }

    function change($dados)
    {
        $colaborador = new Colaborador();
        $colaborador->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $colaborador->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $colaboradores = new Colaborador();
        $colaboradores->select_max('ordem')->get();

        $colaborador = new Colaborador();
        foreach ($dados as $key => $value)
        {
            $colaborador->$key = $value;
        }
        $colaborador->ordem = $colaborador->ordem + 1;
        
        $colaborador->created = time();
        if($colaborador->save())
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $colaborador = new Colaborador();
        $colaborador->where('id', $id)->get();
        if($colaborador->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $colaborador = new Colaborador();
            $colaborador->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($colaborador->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}