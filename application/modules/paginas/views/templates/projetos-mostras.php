<div class="conteudo-projetos projetos-mostras">
	<div class="interna">
		<div class="projetos-lista-slider-wrapper">
			<div class="projetos-lista-sliderrr">
				<div role="projeto-slide">
					<div class="projetos-lista-thumbs-wrapper">
						<a href="<?php echo site_url('projetos/mostras/fiaflora-2008') ?>" class="projeto-thumb">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/mostras/1_fiaflora_2008/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">FiaFlora 2008</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/mostras/mesas-decoradas-d-d') ?>" class="projeto-thumb mesas">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/mostras/2_mesas_decoradas_d_d/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Mesas Decoradas D&D</span>
							</span>
						</a>
						<a href="<?php echo site_url('projetos/mostras/casa-cor-2009') ?>" class="projeto-thumb casa">
							<span class="projeto-thumb-img">
								<img src="<?php echo base_url('assets/img/projetos/mostras/3_casa_cor_2009/2-projetos.jpg') ?>" alt="">
							</span>
							<span class="projeto-thumb-hover">
								<span class="projeto-titulo">Casa Cor 2009</span>
							</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div> 
</div>
<div class="clearfix"></div>