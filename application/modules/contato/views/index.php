<div class="conteudo-contato">
    <div class="interna">
       <div class="contatos left">
            <span class="telefone"><?=$contato->ddd; ?> <?=$contato->telefone; ?></span>
            <div class="clearfix"></div>
            <div class="clearfix"></div>
            <p class="endereco"><?=$contato->endereco; ?><br>
            <?=$contato->cep; ?> &middot; <?=$contato->bairro; ?> &middot; <?=$contato->cidade; ?>,
            <?=$contato->uf; ?>
            </p>
            <span class="email"><a href="mailto://<?=$contato->email ?>"><?=$contato->email; ?></a></span>
        </div>
        <div class="form left">
                        <?php echo form_open('', 'id="contato-form"'); ?>
            <div class="inputs">
                <?=form_input(array('name'=>'nome','value'=>'','class'=>'nome_form textbox', 'placeholder'=>'Nome', 'title'=>'Nome'))?><br />
                <?=form_input(array('name'=>'email','value'=>'','class'=>'email_form textbox', 'placeholder'=>'Email', 'title'=>'Email'))?><br>
                <?=form_input(array('name'=>'telefone','value'=>'','class'=>'telefone_form textbox', 'placeholder'=>'Telefone', 'title'=>'Telefone'))?><br>
            </div>
            <div class="textarea">
                <?=form_textarea(array('name'=>'mensagem','value'=>'','class'=>'mensagem_form mensagem_contato textbox', 'placeholder'=>'Mensagem', 'title'=>'mensagem'))?><br />
                <input type="submit" name="submit" value="enviar" id="contato-submit">
                <?=form_close("\n")?>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>
<div class="clearfix"></div>
