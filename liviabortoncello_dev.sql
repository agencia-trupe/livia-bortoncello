-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 09/08/2013 às 13h58min
-- Versão do Servidor: 5.5.32
-- Versão do PHP: 5.4.15-1~precise+1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `livia_dev`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `categorias`
--

CREATE TABLE IF NOT EXISTS `categorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `created` int(10) NOT NULL,
  `updated` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Extraindo dados da tabela `categorias`
--

INSERT INTO `categorias` (`id`, `ordem`, `titulo`, `slug`, `created`, `updated`, `user_id`) VALUES
(10, 0, 'Arquitetura', 'arquitetura', 1373649037, 0, 1),
(11, 1, 'Interiores', 'interiores', 1373649152, 0, 1),
(12, 2, 'Mostras', 'mostras', 1373649195, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `session_id` varchar(40) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `ip_address` varchar(16) COLLATE utf8_bin NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `clipping`
--

CREATE TABLE IF NOT EXISTS `clipping` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `midia_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Extraindo dados da tabela `clipping`
--

INSERT INTO `clipping` (`id`, `imagem`, `titulo`, `midia_id`) VALUES
(1, '2.jpg', 'Teste', 2),
(2, '21.jpg', 'Teste', 3),
(3, '22.jpg', 'Teste', 4),
(4, '23.jpg', 'Teste', 5),
(5, '24.jpg', 'Teste', 6),
(6, '3.jpg', 'Teste', 6),
(7, '4.jpg', 'Teste', 6),
(8, '25.jpg', 'Teste', 7),
(9, '26.jpg', 'Teste', 8),
(10, '18.jpg', 'Teste', 9),
(11, '27.jpg', 'Teste', 9),
(14, '22.jpg', 'Teste', 11),
(15, '12.jpg', 'Teste', 11),
(17, '25.jpg', 'Teste', 12),
(18, '26.jpg', 'Teste', 13),
(19, '27.jpg', 'Teste', 14),
(20, '28.jpg', 'Teste', 15),
(21, '3.jpg', 'Teste', 15),
(22, '4.jpg', 'Teste', 15);

-- --------------------------------------------------------

--
-- Estrutura da tabela `colaboradores`
--

CREATE TABLE IF NOT EXISTS `colaboradores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `texto` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `colaboradores`
--

INSERT INTO `colaboradores` (`id`, `nome`, `imagem`, `texto`, `created`, `updated`, `ordem`) VALUES
(1, 'Flavia Burcatovsky', 'flavia-burcatovsky11.jpg', '<p><span>Estudante do 5&ordm; ano de Arquitetura e Urbanismo na AEAUSP - Escola da Cidade</span></p>', 1372944602, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `contatos`
--

CREATE TABLE IF NOT EXISTS `contatos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `endereco` varchar(400) NOT NULL,
  `bairro` varchar(255) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `uf` varchar(2) NOT NULL,
  `email` varchar(255) NOT NULL,
  `ddd` int(3) NOT NULL,
  `ddd2` int(3) NOT NULL,
  `telefone` varchar(12) NOT NULL,
  `telefone2` varchar(255) NOT NULL,
  `cep` varchar(12) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `contatos`
--

INSERT INTO `contatos` (`id`, `endereco`, `bairro`, `cidade`, `uf`, `email`, `ddd`, `ddd2`, `telefone`, `telefone2`, `cep`, `twitter`, `facebook`) VALUES
(1, 'Rua Marcelo Gama, 1412, cj602', 'bairro', 'Porto Alegre', 'RS', 'contato@liviabortoncello.com.br', 11, 11, '3342.2600', '3322.5055', '90540-041', 'teste', 'http://facebook.com/teste');

-- --------------------------------------------------------

--
-- Estrutura da tabela `fotos`
--

CREATE TABLE IF NOT EXISTS `fotos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `projeto_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Extraindo dados da tabela `fotos`
--

INSERT INTO `fotos` (`id`, `projeto_id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(2, 1, 'Teste', '11.jpg', 0, 1373654599, NULL),
(3, 1, 'Teste', '4.jpg', 0, 1373654612, NULL),
(4, 1, 'Teste', '31.jpg', 0, 1373654622, NULL),
(5, 1, 'Teste', '21.jpg', 0, 1373654662, NULL),
(6, 1, 'Teste', '12.jpg', 0, 1373654681, NULL),
(7, 25, 'Teste', '14.jpg', 0, 1373662354, NULL),
(8, 25, 'Teste', '24.jpg', 0, 1373662359, NULL),
(9, 26, 'Teste', '25.jpg', 0, 1373917066, NULL),
(10, 27, 'Teste', '26.jpg', 0, 1373917298, NULL),
(11, 28, 'Teste', '27.jpg', 0, 1373917354, NULL),
(12, 29, 'Teste', '28.jpg', 0, 1373917501, NULL),
(13, 30, 'Teste', '29.jpg', 0, 1373917556, NULL),
(14, 31, 'Teste', '210.jpg', 0, 1373917578, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(40) COLLATE utf8_bin NOT NULL,
  `login` varchar(50) COLLATE utf8_bin NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `midia`
--

CREATE TABLE IF NOT EXISTS `midia` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `midia`
--

INSERT INTO `midia` (`id`, `titulo`, `imagem`, `ordem`, `created`, `updated`) VALUES
(10, 'Teste', '14.jpg', 0, 1373900529, 0),
(12, 'Teste 2', '15.jpg', 0, 1373905963, 1373905963),
(13, 'Teste 3', '16.jpg', 0, 1373906032, 1373906032),
(14, 'Teste 4', '17.jpg', 0, 1373906056, 1373906056),
(15, 'Teste 5', '18.jpg', 0, 1373906075, 1373906075);

-- --------------------------------------------------------

--
-- Estrutura da tabela `paginas`
--

CREATE TABLE IF NOT EXISTS `paginas` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `texto` text,
  `imagem` varchar(255) DEFAULT NULL,
  `ativo` tinyint(1) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `template` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Extraindo dados da tabela `paginas`
--

INSERT INTO `paginas` (`id`, `titulo`, `texto`, `imagem`, `ativo`, `created`, `updated`, `slug`, `description`, `template`) VALUES
(19, 'O escritório', '<p>Livia Bortoncello Arquitetura tem como foco principal o desenvolvimento de projetos arquitet&ocirc;nicos residenciais e comerciais e projetos de inteirores bem detalhados inclusive com design de mobili&aacute;rio.</p>\n<h2>Os projetos nascem de instenso di&aacute;logo com os clientes de onde surgem as id&eacute;ias e conceitos que ser&atilde;o as diretrizes prioncipais de cada projeto. Estas diretrizes funcionam como a base do processo criativo.</h2>\n<p>Baseado em Porto Alegre RS h&aacute; mais de duas d&eacute;cadas o escrit&oacute;rio concentra sua pr&aacute;tica no sul do Brasil e tamb&eacute;m desenvolve projetos em outras regi&otilde;es do pa&iacute;s e no exterior.</p>', 'perfil-livia.jpg', NULL, NULL, 0, 'escritorio', '', 'escritorio');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projetos`
--

CREATE TABLE IF NOT EXISTS `projetos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `subcategoria_id` int(11) NOT NULL,
  `titulo` varchar(255) DEFAULT NULL,
  `data` varchar(255) NOT NULL,
  `descricao` varchar(255) NOT NULL,
  `capa` varchar(255) DEFAULT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Extraindo dados da tabela `projetos`
--

INSERT INTO `projetos` (`id`, `ordem`, `categoria_id`, `subcategoria_id`, `titulo`, `data`, `descricao`, `capa`, `created`, `updated`) VALUES
(30, 0, 11, 32, 'Teste Interiores Comercial', '', '0', '19.jpg', 1373917548, NULL),
(32, 0, 11, 31, 'Teste', '', '0', 'livia1.jpg', 1374085181, NULL),
(33, 0, 10, 27, 'Teste', 'Maio de 2010', 'Apartamento 300m²', '1600x9003.jpg', 1374779284, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) NOT NULL,
  `default` tinyint(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `roles`
--

INSERT INTO `roles` (`id`, `role`, `default`) VALUES
(1, 'admin', 1),
(2, 'user', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `servicos`
--

CREATE TABLE IF NOT EXISTS `servicos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `descricao` text,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `slides`
--

CREATE TABLE IF NOT EXISTS `slides` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `titulo` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `imagem` varchar(255) DEFAULT NULL,
  `padrao` tinyint(1) NOT NULL,
  `ordem` int(11) NOT NULL,
  `created` int(10) DEFAULT NULL,
  `updated` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `slides`
--

INSERT INTO `slides` (`id`, `titulo`, `link`, `imagem`, `padrao`, `ordem`, `created`, `updated`) VALUES
(15, 'teste', 'http://teste.com', 'livia-05821.jpg', 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Estrutura da tabela `subcategorias`
--

CREATE TABLE IF NOT EXISTS `subcategorias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL,
  `categoria_id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `created` int(10) NOT NULL,
  `updated` int(10) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

--
-- Extraindo dados da tabela `subcategorias`
--

INSERT INTO `subcategorias` (`id`, `ordem`, `categoria_id`, `slug`, `titulo`, `created`, `updated`, `user_id`) VALUES
(27, 0, 10, 'casas', 'Casas', 1373649097, 0, 1),
(28, 1, 10, 'condominios', 'Condomínios', 1373649104, 0, 1),
(29, 2, 10, 'comerciais', 'Comerciais', 1373649132, 0, 1),
(30, 3, 10, 'edificios', 'Edifícios', 1373649136, 0, 1),
(31, 0, 11, 'residencial', 'Residencial', 1373649172, 0, 1),
(32, 1, 11, 'comercial', 'Comercial', 1373649178, 0, 1),
(33, 0, 12, 'mostras', 'Mostras', 1373649206, 0, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(100) COLLATE utf8_bin NOT NULL,
  `role_id` int(11) NOT NULL,
  `activated` tinyint(1) NOT NULL DEFAULT '1',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `ban_reason` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `new_password_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `new_password_requested` datetime DEFAULT NULL,
  `new_email` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `new_email_key` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `role_id`, `activated`, `banned`, `ban_reason`, `new_password_key`, `new_password_requested`, `new_email`, `new_email_key`, `last_ip`, `last_login`, `created`, `modified`) VALUES
(1, 'trupe', '$P$Bm.afp74faAjfqPGOJPYzFREhWoN/S/', 'nilton@trupe.net', 1, 1, 0, NULL, NULL, NULL, NULL, NULL, '127.0.0.1', '2013-07-30 13:05:12', '2013-03-06 15:59:08', '2013-07-30 13:05:12');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_autologin`
--

CREATE TABLE IF NOT EXISTS `user_autologin` (
  `key_id` char(32) COLLATE utf8_bin NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `user_agent` varchar(150) COLLATE utf8_bin NOT NULL,
  `last_ip` varchar(40) COLLATE utf8_bin NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`key_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Estrutura da tabela `user_profiles`
--

CREATE TABLE IF NOT EXISTS `user_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `country` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=4 ;

--
-- Extraindo dados da tabela `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `country`, `website`) VALUES
(1, 1, NULL, NULL),
(2, 0, NULL, NULL),
(3, 0, NULL, NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
